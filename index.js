document.querySelectorAll('nav ul li a').forEach(item => {
    item.addEventListener('click', () => {
      document.querySelectorAll('nav ul li a').forEach(link => {
        link.classList.remove('clicked');
      });
      item.classList.add('clicked');
    });
    
  });
  
  document.querySelectorAll('.down-arical div h3').forEach(item => {
    item.addEventListener('mouseenter', () => {
        item.style.color = 'rgb(135,224,170)'; 
    });
    item.addEventListener('mouseleave', () => {
      item.style.color = ''; 
  });
});
document.querySelectorAll('.footer-navigation-1 a,.footer-navigation-2 a').forEach(item => {
  item.addEventListener('mouseenter', () => {
      item.style.color = 'rgb(135,224,170)'; 
  });
  item.addEventListener('mouseleave', () => {
    item.style.color = ''; 
});

});
document.querySelectorAll('.image').forEach(item => {
  item.addEventListener('mouseenter', (event) => {
      changeColor(event.target, 'rgb(135,224,170)');
  });
  item.addEventListener('mouseleave', (event) => {
      changeColor(event.target, ''); 
  });
});

function changeColor(element, color) {
  element.style.filter = color ? 'invert(50%) sepia(5%) saturate(3411%) hue-rotate(97deg) brightness(96%) contrast(91%)' : ''; 
}



document.addEventListener("DOMContentLoaded", function() {
  const btn = document.getElementById("btn");
  const cancel = document.getElementById("cancel");
  const imageContainer = document.querySelector(".image-container");

  btn.addEventListener("click", toggleImageContainer);
  cancel.addEventListener("click", toggleImageContainer);

  function toggleImageContainer() {
      if (imageContainer.style.display === "none") {
          imageContainer.style.display = "block";
      } else {
          imageContainer.style.display = "none";
      }
  }
});


